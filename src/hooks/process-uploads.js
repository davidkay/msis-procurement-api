// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    const { files } = context.params;

    
    // add createdAt to DB entry
    context.data.createdAt = new Date().toLocaleString('en-US');
    context.data.files = files.map(filename => filename.originalname);

    return context;
  };
};
