

const processUploads = require('../../hooks/process-uploads');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [processUploads()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
