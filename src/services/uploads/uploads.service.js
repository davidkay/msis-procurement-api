// Initializes the `uploads` service on path `/uploads`
const createService = require('feathers-nedb');
const createModel = require('../../models/uploads.model');
const hooks = require('./uploads.hooks');

const multer = require('multer');
const upload = multer({ dest: 'uploads/' });

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');
  // const storage = multer.diskStorage({
  //   destination: function(req,file,callback) {
  //     callback(null,'./uploads');
  //   },
  //   filename: function(req,file,callback) {
  //     callback(null,Date.now()+'-'+file.originalname);
  //   }
  // });

  const options = {
    Model,
    paginate
  };

  app.use('/uploads',
  
    // multer parses the 'uri' array.
    // current max number of items in array is 5
    // create the service uploads which saves files and

    upload.array('uri', 5),

    function (req, res, next) {
      req.feathers.files = req.files;
      next();
    },    
    
    createService(options)
  );
  
  // Get our initialized service so that we can register hooks
  const service = app.service('uploads');

  service.hooks(hooks);
};
